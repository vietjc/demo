FROM python:3.4-alpine
ADD . /code
WORKDIR /code
RUN pip install -r rq.txt
CMD ["python","app.py"] 
