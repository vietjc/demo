Luồng chạy CI/CD của ứng dụng

1. Luồng CI/CD với Gitlab CI
Khi merge request vào nhánh test hoặc nhánh master, file gitlab-ci.yml sẽ kích hoạt luồng build và deploy ứng dụng
1.1. stage build sẽ chạy file Dockerfile để build app và đẩy lên gitlab registry
1.2. stage deploy sẽ ssh vào remote server:
   - docker login vào private registry
   - chạy file deploy.sh trên remote server để deploy ứng dụng (bằng docker swarm hoặc k8s)

Project demo này thực hiện deploy bằng docker-compose hoặc docker stack

2. Luồng CI/CD với Jenkins
Trên jenkins thực hiện Build with parameter, truyền vào param cần thiết để build và deploy giống gitlab ci